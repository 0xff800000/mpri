import librosa
from librosa import load
from hmmlearn import hmm
import numpy as np
import sklearn
import warnings

warnings.filterwarnings('ignore')

SR = 16000  # Audio sampling rate
HOP_LENGTH = 128  # Size of a frame (8ms at 16kHz)
NB_COEFFICIENT = 12  # For MFCC


def mfcc(y):
    """
    Apply Mel-frequency cepstral coefficients (MFCCs) on a audio time serie.
    Hypothesis:
    - Sampling rate is at costant sampling rate (SR)
    """
    mfccs = librosa.feature.mfcc(y, SR, n_mfcc=NB_COEFFICIENT, hop_length=HOP_LENGTH)
    mfccs = sklearn.preprocessing.scale(mfccs, axis=1)  # Scale to unit variance and zero mean
    return mfccs.transpose()  # To have frames on the 0 axis


def print_duration_nb_accoustic_models(time_series, cepstr):
    # TODO
    # example of possible result:
    # Duration [ms]: 962.3125
    for i in range(0, len(time_series)):
        print("Duration [ms]: ", time_series[i].size * 1 / SR * 1000)
        # Nb. accoustic vectors: 121
        print("Nb. accoustic vectors: ", cepstr[i].shape[0])


def concatenate_cepstrums(dataset):
    """
    hmmlearning can ingest a long serie containing multiple series
    you need to pass the serie and length of each
    """
    X = np.concatenate(dataset)
    lengths = [c.shape[0] for c in dataset]
    return X, lengths


def test(dataset, models):
    X, lengths = concatenate_cepstrums(dataset)
    scores = [m.score(X, lengths) for m in models]
    print(scores)
    print(f"Best model: {np.argmax(scores) + 1}")


# The audio files are in a 'wave' folder
# Train set
train_1 = [load("wave/1_1.wav", SR)[0], load("wave/1_2.wav", SR)[0], load("wave/1_3.wav", SR)[0]]  # "un"
train_2 = [load("wave/2_1.wav", SR)[0], load("wave/2_2.wav", SR)[0], load("wave/2_3.wav", SR)[0]]  # "deux"
train_3 = [load("wave/3_1.wav", SR)[0], load("wave/3_2.wav", SR)[0], load("wave/3_3.wav", SR)[0]]  # ...
train_4 = [load("wave/4_1.wav", SR)[0], load("wave/4_2.wav", SR)[0], load("wave/4_3.wav", SR)[0]]
train_5 = [load("wave/5_1.wav", SR)[0], load("wave/5_2.wav", SR)[0], load("wave/5_3.wav", SR)[0]]

# Test set
test_1 = [load("wave/1t.wav", SR)[0]]  # "un"
test_2 = [load("wave/2t.wav", SR)[0]]  # "deux"
test_3 = [load("wave/3t.wav", SR)[0]]  # ...
test_4 = [load("wave/4t.wav", SR)[0]]
test_5 = [load("wave/5t.wav", SR)[0]]
# test_p = [load("wave/peu.wav", SR)[0]]

train_1_c = list(map(mfcc, train_1))
train_2_c = list(map(mfcc, train_2))
train_3_c = list(map(mfcc, train_3))
train_4_c = list(map(mfcc, train_4))
train_5_c = list(map(mfcc, train_5))

test_1_c = list(map(mfcc, test_1))
test_2_c = list(map(mfcc, test_2))
test_3_c = list(map(mfcc, test_3))
test_4_c = list(map(mfcc, test_4))
test_5_c = list(map(mfcc, test_5))
# test_p_c = list(map(mfcc, test_p))

print("Train 1:")
print_duration_nb_accoustic_models(train_1, train_1_c)
print("Train 2:")
print_duration_nb_accoustic_models(train_2, train_2_c)
print("Train 3:")
print_duration_nb_accoustic_models(train_3, train_3_c)
print("Train 4:")
print_duration_nb_accoustic_models(train_4, train_4_c)
print("Train 5:")
print_duration_nb_accoustic_models(train_5, train_5_c)

N = 50
X, lengths = concatenate_cepstrums(train_1_c)
model_1 = hmm.GaussianHMM(n_components=N)
model_1.fit(X, lengths)

N = 53
X, lengths = concatenate_cepstrums(train_2_c)
model_2 = hmm.GaussianHMM(n_components=N)
model_2.fit(X, lengths)

N = 60
X, lengths = concatenate_cepstrums(train_3_c)
model_3 = hmm.GaussianHMM(n_components=N)
model_3.fit(X, lengths)

N = 65
X, lengths = concatenate_cepstrums(train_4_c)
model_4 = hmm.GaussianHMM(n_components=N)
model_4.fit(X, lengths)

N = 125
X, lengths = concatenate_cepstrums(train_5_c)
model_5 = hmm.GaussianHMM(n_components=N)
model_5.fit(X, lengths)

# print("1")
# test(test_1_c, [model_1, model_2, model_3, model_4, model_5])
#
# print("2")
# test(test_2_c, [model_1, model_2, model_3, model_4, model_5])
#
# print("3")
# test(test_3_c, [model_1, model_2, model_3, model_4, model_5])
#
# print("4")
# test(test_4_c, [model_1, model_2, model_3, model_4, model_5])
#
# print("5")
# test(test_5_c, [model_1, model_2, model_3, model_4, model_5])

#######################################################################################################################
# New model LR Transition Probabilities

N = 50
X, lengths = concatenate_cepstrums(train_1_c)
model_1_lr = hmm.GaussianHMM(n_components=N, covariance_type="diag", init_params="cm", params="cmt")
model_1_lr.startprob_ = np.zeros(N)
model_1_lr.startprob_[0] = 1
model_1_lr.transmat_ = (np.diagflat(np.ones(99), 1)+ np.diagflat(np.ones(100), 0))/2
model_1_lr.transmat_[-1,-1] = 1;
model_1_lr.fit(X, lengths)

N = 53
X, lengths = concatenate_cepstrums(train_2_c)
model_2_lr = hmm.GaussianHMM(n_components=N, covariance_type="diag", init_params="cm", params="cmt")
model_2_lr.startprob_ = np.zeros(N)
model_2_lr.startprob_[0] = 1
model_2_lr.transmat_ = np.triu(np.ones((N, N))) / 2
model_2_lr.fit(X, lengths)

N = 60
X, lengths = concatenate_cepstrums(train_3_c)
model_3_lr = hmm.GaussianHMM(n_components=N, covariance_type="diag", init_params="cm", params="cmt")
model_3_lr.startprob_ = np.zeros(N)
model_3_lr.startprob_[0] = 1
model_3_lr.transmat_ = np.triu(np.ones((N, N))) / 2
model_3_lr.fit(X, lengths)

N = 65
X, lengths = concatenate_cepstrums(train_4_c)
model_4_lr = hmm.GaussianHMM(n_components=N, covariance_type="diag", init_params="cm", params="cmt")
model_4_lr.startprob_ = np.zeros(N)
model_4_lr.startprob_[0] = 1
model_4_lr.transmat_ = np.triu(np.ones((N, N))) / 2
model_4_lr.fit(X, lengths)

N = 125
X, lengths = concatenate_cepstrums(train_5_c)
model_5_lr = hmm.GaussianHMM(n_components=N, covariance_type="diag", init_params="cm", params="cmt")
model_5_lr.startprob_ = np.zeros(N)
model_5_lr.startprob_[0] = 1
model_5_lr.transmat_ = np.triu(np.ones((N, N))) / 2
model_5_lr.fit(X, lengths)


print("1")
print(model_1_lr.monitor_.converged)
test(test_1_c, [model_1_lr, model_2_lr, model_3_lr, model_4_lr, model_5_lr])

print("2")
print(model_2_lr.monitor_.converged)
test(test_2_c, [model_1_lr, model_2_lr, model_3_lr, model_4_lr, model_5_lr])

print("3")
print(model_3_lr.monitor_.converged)
test(test_3_c, [model_1_lr, model_2_lr, model_3_lr, model_4_lr, model_5_lr])

print("4")
print(model_4_lr.monitor_.converged)
test(test_4_c, [model_1_lr, model_2_lr, model_3_lr, model_4_lr, model_5_lr])

print("5")
print(model_5_lr.monitor_.converged)
test(test_5_c, [model_1_lr, model_2_lr, model_3_lr, model_4_lr, model_5_lr])
