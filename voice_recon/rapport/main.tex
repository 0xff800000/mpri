%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Lachaise Assignment
% LaTeX Template
% Version 1.0 (26/6/2018)
%
% This template originates from:
% http://www.LaTeXTemplates.com
%
% Authors:
% Marion Lachaise & François Févotte
% Vel (vel@LaTeXTemplates.com)
%
% License:
% CC BY-NC-SA 3.0 (http://creativecommons.org/licenses/by-nc-sa/3.0/)
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%----------------------------------------------------------------------------------------
%	PACKAGES AND OTHER DOCUMENT CONFIGURATIONS
%----------------------------------------------------------------------------------------

\documentclass{article}

\input{structure.tex} % Include the file specifying the document structure and custom commands

%----------------------------------------------------------------------------------------
%	ASSIGNMENT INFORMATION
%----------------------------------------------------------------------------------------

\title{MPRI : TP2 Reconnaissance de la parole − HMMs} % Title of the assignment

\author{Dan Yvan Baumgartner\\ \texttt{dan.baumgartner@master.hes-so.ch} \\ Maël Jaquier\\ \texttt{mael.jaquier@master.hes-so.ch}} % Author name and email address

\date{HES-SO MSE --- \today} % University, school and/or department name(s) and a date

%----------------------------------------------------------------------------------------

\begin{document}

\maketitle % Print the title

%----------------------------------------------------------------------------------------
%	INTRODUCTION
%----------------------------------------------------------------------------------------

\section{Préparation des données et extraction des paramètres}
Dans cette première partie, il s'agit d'enregister les fichiers audio qui permettront de tester et entraîner les modèles HMMs. Ces fichiers ont été ensuite importés dans le script Python à l'aide de la bibliothèque \textit{librosa}, ce qui a permit d'extraire différents paramètres.
Le \autoref{tab:audio} montre les divers mots enregistrés, leur retranscription en alphabet phonétique international, leur nombre d'états HMM, leurs durée moyenne ainsi que le nombre de vecteurs acoustiques. Le nombre d'états $N$ a été déterminé à partir du nombre de phonèmes : $N=n_{phonemes}+2$. De cette manière, le modèle peut générer des états pour les silences du début et de la fin du mot.\\

\begin{minipage}{\linewidth}
\centering
\captionof{table}{Fichiers audio} \label{tab:audio}
\begin{tabular}{ C{1.25in} C{.85in} *4{C{.75in}}}\toprule[1.5pt]
	    \bf Mot & \bf Transcription phonétique & \bf $N$ & \bf Durée [ms] & \bf $n$ vect acoust \\ \hline
			Un & \textipa{/\~E/} & 3 & 786 & 99 \\ \hline
	    Deux & \textipa{/d\o/} & 4 & 929 & 117 \\ \hline
			Trois & \textipa{/t\textinvscr wa/} & 6 & 979 & 123 \\ \hline
	    Quatre & \textipa{/kat\textinvscr/} & 6 & 918 & 115 \\ \hline
	    Cinq & \textipa{/s\~Ek/} & 5 & 1122 & 141 \\ \hline
\bottomrule[1.25pt]
\end {tabular}\par
\bigskip
\end{minipage}


\section{Entraînement des modèles}
L'entraînement des modèles se fait à l'aide de la bibliothèque \texttt{hmmlearn}. Cette dernière possède une fonction \texttt{hmm.GaussianHMM()} qui permet de créer des modèles de Markov cachés gaussiens. Dans ce premier entraînement, les modèles utilisés sont paramétrés par défaut à l'exception du nombre d'états $N$. Il est également possible d'afficher la probabilité en fonction de l'itération en passant \texttt{verbose=True} à la fonction. La \autoref{figure:converg:un} montre la convergence pour le mot "un". On constate que la fonction est strictement croissante, ce qui indique que le modèle converge.

\begin{figure}[H]
	\centering
	\includegraphics[width=.4\linewidth]{monitor_un.png}
	\caption{Convergence pour le mot "un"}
 \label{figure:converg:un}
\end{figure}


\section{Modèle de Markov caché Left-Right}

Pour utiliser le Modèle de Markov caché Left-Right, il faut d'abord initialiser le vecteur de probabilité initiale ${\bf \pi}$ (\texttt{model.startprob}).

\begin{equation}
\pi_i =
\begin{cases}
1 & \text{if } i=1\\
0 & \text{if } i\neq1\\
\end{cases}
, i = 1 \ldots N
\end{equation}

Il faut également initialiser les éléments de la matrice de transition $A$ de manière à ce que le modèle ne peux que changer d'état en se déplaçant sur la gauche ou en restant dans le même état (\texttt{model.transmat}).

\begin{equation}
A_{ij} =
\begin{cases}
1 & \text{if } i=j=N\\
0.5 & \text{if } i=j \text{ or } i+1=j\\
0 & \text{otherwise}\\
\end{cases}
\end{equation}

La \autoref{figure:converg:diff} montre la différence de convergence entre le modèle par défaut et le modèle "Left-Right" pour le mot "un". 
\begin{figure}[H]
	\centering
	\includegraphics[width=.4\linewidth]{monitor_diff.png}
	\caption{Différence de convergence entre le modèle par défaut et le "Left-Right"}
 \label{figure:converg:diff}
\end{figure}

\begin{question}
	Que pouvez-vous conclure de cette evolution ?
\end{question}

Pour les deux modèles utilisés, l'évolution de la probabilité était strictement croissante, ce qui indique que les modèles convergeaient. On constate que la convergence du modèle par défaut est légèrement plus rapide que pour le "Left-Right", mais cela reste négligeable.

\section{Test des modèles}

La \autoref{figure:prob:matrix1} montre les résultats des probabilités pour chaque nombre. L'axe $y$ represente les vrais labels, tandis que l'axe $x$ représente les labels prédis par les modèles. On constate que les éléments de la diagonale sont dominants dans leur ligne respectives. 

\begin{question}
		Quels sont les modèles reconnus pour vos 5 fichiers de test ?
\end{question}

Comme la diagonale domine pour chaque ligne, cela indique que tous les modèles ont réussi à prédir le bon nombre.

\begin{figure}[H]
	\centering
	\includegraphics[width=.4\linewidth]{prob_matrix.png}
	\caption{Tableau de probabilité pour chaque nombre}
 \label{figure:prob:matrix1}
\end{figure}

Le test de reconnaissance a été appliqué sur un mot qui n'est pas présent dans le training set (le mot "peu"). Les résultats sont montrés dans la \autoref{figure:prob:peu}. 

\begin{figure}[H]
	\centering
	\includegraphics[width=.4\linewidth]{prob_peu.png}
	\caption{Probabilité du mot "peu"}
 \label{figure:prob:peu}
\end{figure}

\begin{question}
	Quel est le modèle gagnant ?
\end{question}

On constate que le système a déterminé qu'il s'agit d'un "un". Cela souligne le fait que l'implémentation de ce système ne peut pas déterminer si le mot enregistré est nouveau.

\begin{question}
	Quel est le résultat avec les données d'un collègue ?
\end{question}

La \autoref{figure:prob:matrix2} montre que la reconnaissance prédit "deux" pour "un", "deux" et "trois" et la prédiction est juste pour "quatre" et "cinq". Cela montre que le modèle est entraîné pour une personne spécifique. Comme avec le mot "peu", il n'est pas capable de détecter une nouvelle personne. Pour créer un système de reconnaissance de chiffres plus efficace, il faudrait l'entraîner avec les données d'un grand nombre de personnes.

\begin{figure}[H]
	\centering
	\includegraphics[width=.4\linewidth]{prob_matrix_coll.png}
	\caption{Tableau de probabilité pour chaque nombre pour les données du collègue}
 \label{figure:prob:matrix2}
\end{figure}

\begin{question}
	Comment créer un système de classification binaire biométrique ?
\end{question}

Afin d'obtenir un système de classification "OUI" ou "NON", nous pourrions utiliser la séquence de "un", "deux", "trois", "quatre" et "cinq". On donne les cinqs éléments à chaque modèle personnel.\\
Le modèle donnant le meilleur résultat (le plus de nombre juste) correspond à la personne enregistrée. Un seuil minimum est par contre nécessaire pour éviter les faux positifs en cas de personne non-enregistrée (juste > 20\%) car le système tend à répondre un chiffre unique si les données ne sont pas connues. 

\end{document}
